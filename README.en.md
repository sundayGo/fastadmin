Vulnerability exists /assets/js/require-backend.min.js	6210-6216行

There is a controllable variable fullurl in the onerror event, and the XSS code can be triggered by loading the image path

```javascript
    var Upload = {
            list: {},
            config: {
                container: document.body,
                classname: '.plupload:not([initialized])',
                previewtpl: '<li class="col-xs-3"><a href="<%=fullurl%>" data-url="<%=url%>" target="_blank" class="thumbnail"><img src="<%=fullurl%>" onerror="this.src=\'' + Fast.api.fixurl("ajax/icon") + '?suffix=\'+\'<%=fullurl%>\'.split(\'.\').pop();this.onerror=null;" class="img-responsive"></a><a href="javascript:;" class="btn btn-danger btn-xs btn-trash"><i class="fa fa-trash"></i></a></li>',
            }
```

Login member, edit personal information, modify member avatar, picture path avatar parameter can be controlled

application\api\controller\User.php	147-166行

```php
    public function profile()
    {
        $user = $this->auth->getUser();
        $username = $this->request->request('username');
        $nickname = $this->request->request('nickname');
        $bio = $this->request->request('bio');
        $avatar = $this->request->request('avatar', '', 'trim,strip_tags,htmlspecialchars');
        if ($username) {
            $exists = \app\common\model\User::where('username', $username)->where('id', '<>', $this->auth->id)->find();
            if ($exists) {
                $this->error(__('Username already exists'));
            }
            $user->username = $username;
        }
        $user->nickname = $nickname;
        $user->bio = $bio;
        $user->avatar = $avatar;
        $user->save();
        $this->success();
    }
```

Enter the constructed JavaScript code when modifying the avatar: #‘;alert(1);//

![1](/1.png)

When the administrator edits the member's personal information in the background, assets/js/require-backend.min.js will be loaded. The fullurl variable refers to the path of the member's avatar picture, so that the onerror event is triggered and the javascript code is executed

![2](/2.png) 

